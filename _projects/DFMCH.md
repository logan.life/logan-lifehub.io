---
classes: wide
title: "Department of Family Medicine and Community Health"
excerpt: "Content and brand management for an academic medical department at the University of Pennsylvania"
header:
  image: /assets/images/DFMCHprojectheader.jpg
  teaser: /assets/images/DFMCH_Logo_Acronym.png
sidebar:
  - title: "Role"
    text: "Project Manager, Content Manager"
  - title: "Responsibilities"
    text: "Manage client side inputs during design and development, package and manage all content from deployment forward."
  - title: "Key Stuff I Learned"
    text: "What makes a good RFP. How to define scope and avoid creep. How to solicit feedback and integrate vision from diverse, high-profile stakeholders. "

---
[Link to Project](https://www.med.upenn.edu/fmch/){: .btn .btn--large .btn--info}<br><br>
This academic medical department at the University of Pennsylvania needed a complete brand and marketing overhaul, including logos, a comprehensive design system, an audience-aware website, print materials, and more. I hired local Philadelphia company [P'unk Avenue](https://www.punkave.com/) and managed all aspects of the project from stakeholder interviews during development through content creation and bug testing.


In addition to project management, I am responsible for all digital assets and content, ongoing site management, print material design and production, future feature development, etc.
