---
layout: single
permalink: /about/
classes: wide
author_profile: true
---
<h1>Hi! I'm Logan.</h1>

In 2016, after investing 10 years in the restaurant industry working my way up to General Manager, I decided it was time for a change. I was 32 and could boast a shallow background in HTML and CSS along with a smattering of PHP, MySQL, Python, and Javascript. I made the choice to re-skill into a career in coding.

I've chosen both formal and informal education - an aggressive mix of self-taught online courses combined with enrolling at the University of Pennsylvania. There are many viable strategies for re-skilling into coding - this is just the one that feels right to me.


<!-- My master plan was pretty simple:

{: .notice--info}

1. Quit the restaurant industry and secure a job at an Ivy League University that provides free tuition to employees.
2. Earn a BA + sub-matriculate into a Masters in Computer Science.
  - Include self-taught online classes along the way to stay ahead of the curve and develop a killer portfolio.
  - Land my first dev job while still in school to get a jump on banking some work experience.
3. Earn a well-paying position with a progressive and exciting fully remote company.
4. Travel the world with my (supportive and extraordinarily patient) spouse, working from wherever our home of the moment happens to be.


Easy enough, right?

Step 1 is complete. The site you are currently reading is the chronicle of Step 2. -->

If you're working towards a similar goal, I sincerely hope you can find something here to take away and use. I share my progress along with any lessons learned [over here.](/blog/) I house any and all projects [over here.](/projects/) And I'm always eager to get to know other people who are on this journey, so don't hesitate to [reach out.](mailto:"logan.life.online@gmail.com")
