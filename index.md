---
layout: splash
header:
  image: /assets/images/phillyHeaderImage.jpg
  image_description: "A photo featuring light painting over a  Philadelphia street with an iconic bridge in the background. Photo Credit: Osman Rana on Unsplash - https://unsplash.com/photos/6IxGFVz0wP"

---
