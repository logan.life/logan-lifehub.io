---
title: "General Ed Reqs \n| Mary Oliver \n| Marry All Of Her"
excerpt_separator: "<!--more-->"
categories:
  - Blog
tags:
  - Progress Updates
  - Real Life
  - Stuff I'm Reading
  - Mary Oliver
  - Kaveh Akbar
header:
  teaser: /assets/images/cherryBlossomsSchuylkillPhiladelphia.jpg
  image: /assets/images/cherryBlossomsSchuylkillPhiladelphia.jpg
  image_description: "A close-up photograph of pink cherry blossoms on a branch, taken in the Schuylkill River park"
  caption: "Photo Credit: [**Jose Fontano**](https://unsplash.com/photos/zSrVMit8s2s) Schuylkill River Trail, Philadelphia, USA"
classes: wide
---

There's a certain school of thought which suggests that if one is to go, then one should go big - and failing that, one should return to one's home. <!--more--><br/><br/>I would suggest that in the event of deciding to go big, be it in learning to program or really any grand ambition, one should always make sure to bring along a healthy supply of really good poetry.
