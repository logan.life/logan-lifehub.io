---
classes: wide
title: "National Center for Integrated Behavioral Health"
excerpt: "Content and brand management for a new academic unit for primary care training enhancement."
header:
  image: /assets/images/NCIBHprojectheader.jpg
  teaser: /assets/images/NCIBH_Logo_Acronym.png
sidebar:
  - title: "Role"
    text: "Project Manager, Content Manager"
  - title: "Responsibilities"
    text: "Manage client side inputs during design and development, package and manage all content from deployment forward."
  - title: "Key Stuff I Learned"
    text: "How to create a brand and voice from scratch. How to incorporate the specific requirements of federal grant funding mechanisms and ensure project officers are satisfied. How to align the visual language of two distinct sites with different audiences. "
---

[Link to Project](https://www.ncibh.org/){: .btn .btn--large .btn--info}<br><br>
This Center was founded in 2016 via a federal grant from the [Health Resources and Services Administration][6a797102], an operating division of the [U.S. Department of Health and Human Services][24d2fab8].  As a new entity, we needed to start from scratch while ensuring compliance with the requirements of the funding mechanism. I hired local Philadelphia company [P'unk Avenue](https://www.punkave.com/) and managed all aspects of the project from stakeholder interviews during development through content creation and bug testing. This site was developed alongside the [Department of Family Medicine and Community Health](/projects/DFMCH) site. It was designed to feel integrated yet distinct from the Department's site and branding.

  [6a797102]: https://www.hrsa.gov/ "HRSA"
  [24d2fab8]: https://www.hhs.gov/ "HHS"


In addition to project management, I am responsible for all digital assets and content, ongoing site management, print material design and production, future feature development, etc. I also give presentations as a model site for other academic units across the country, including [Harvard](https://oralhealth.hsdm.harvard.edu/), [Northwestern](http://www.feinberg.northwestern.edu/sites/ipham/centers/primary-care-innovation/hrsa-project/index.html), [UC Davis](https://www.ucdmc.ucdavis.edu/workforce-diversity/), and more.
