---
layout: single
permalink: /wedding-photos/
classes: wide
author_profile: true
---

{% for image in site.static_files %}
    {% if image.path contains 'images/wedding' %}
        <img src="{{ site.baseurl }}{{ image.path }}" alt="image" />
    {% endif %}
{% endfor %}
