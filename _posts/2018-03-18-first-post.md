---
title: "Writing the first post is the worst."
excerpt_separator: "<!--more-->"
categories:
  - Blog
tags:
  - Harry Potter
  - Cycle Bust
header:
  teaser: /assets/images/homemadeHPNecklaceteaser.jpg
  image: /assets/images/woodLineSanFrancisco.jpg
  image_description: "A root lined path snakes its way into the distance in a green forest"
  caption: "Photo Credit: [**James Forbes**](https://unsplash.com/photos/jrzvClypPq8) Wood Line, San Francisco, USA"

---

There is, I am sure, a good and proper way to write the first post of a new blog. <br/><br/>This is not that way.

<!--more-->

The thing is, I don't want to try and summon some overwrought ceremonial nonsense to mark the occasion - I'd rather just get going. So in place of that hypothetical exercise in over-earnest pablum, here's a photo of a necklace I made a few weeks ago.


![A close up photograph of a leather and metal cord necklace with a small locket and the sign of the Deathly Hallows as charms.](/assets/images/homemadeHPNecklace.jpg)



Now then. Let's just move on, shall we?
